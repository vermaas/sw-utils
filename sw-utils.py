
import numpy as np
import matplotlib.pyplot as plt
import requests
import json


SW_API = "https://spaceweather.astron.nl/api2/scintillation_preview/tasks"          # the atdb-ldv sdc (production) environment.


def get_fits(my_list):
    fits_file = [f for f in my_list if 'fits' in f][0]
    return fits_file


def get_S4(my_list):
    S4file = [f for f in my_list if 'S4.png' in f][0]
    return S4file


def get_png(my_list):
    S4file = [f for f in my_list if 'png' in f][0]
    return S4file


def get_files(timestamp_start, timestamp_end):
    """
    adapted from script by Maaike Mevius
    """

    myfilter = json.dumps({"$and": [{'sample_start_datetime': {'$lte': str(timestamp_end)}},
                                    {'sample_end_datetime': {'$gte': str(timestamp_start)}}]})
    # myfilter = json.dumps({"OBSERVATION_ID":obsid})
    response = requests.get(SW_API, params={'filter': myfilter})

    all_results = []
    fitsfiles = []
    S4files = []
    while response.status_code == 200:
        print(response)
        response_json = response.json()
        results = response_json['results']

        if len(results) == 0:
            break
        print(f"Number of results: {len(results)}")
        all_results += response_json['results']
        for result in response_json['results'][:]:

            try:
                fitsfiles.append(get_fits(result['additional_files_url']))
            except IndexError:
                # print("No fits here", result['filename'])
                pass
            try:
                S4files.append(get_png(result['additional_files_url']))
            except IndexError:
                # print("No S4 here", result)
                pass
        url = response_json['next']
        response = requests.get(url)
    return S4files, fitsfiles, all_results


def query_spaceweather():
    input_date = input("enter date (default = 2020-04-15)\n")
    if not input_date:
        input_date = "2020-04-15"

    input_time_start = input("enter start time (default = 00:00:00)\n")
    if not input_time_start:
        input_time_start = "00:00:00"

    input_time_end = input("enter end time (default = 23:59:00)\n")
    if not input_time_end:
        input_time_end = "23:59:00"

    timestamp_start = f"{input_date}T{input_time_start}"
    timestamp_end = f"{input_date}T{input_time_end}"

    S4, fits_files, results = get_files(timestamp_start=timestamp_start, timestamp_end=timestamp_end)
    S4data = [i['S4_data'] for i in results]
    plt.plot(np.concatenate(S4data))
    plt.show()

    for png in S4:
        print(png)

    for fits in fits_files:
        print(fits)


if __name__ == '__main__':

    print("sw-utils - version 15 mar 2024")


    #--- choose operation

    answer = input("Choose operation\n"
                   "q: Quit\n"
                   "1: Query\n"
                   )

    print("working...")

    if answer == "1":
        query_spaceweather()

    elif answer == 'q':
        print('quit')